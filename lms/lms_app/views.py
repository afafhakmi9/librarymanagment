from django.shortcuts import redirect, render ,get_object_or_404
from .models import *
from .forms import BookForm ,CategoryForm
# Create your views here.
def index (request):
    if request.method == 'POST':
        add_book = BookForm(request.POST,request.FILES)
        if add_book.is_valid():
            add_book.save()
        add_category = CategoryForm(request.POST)
        if add_category.is_valid():
            add_category.save()    

    context={
        'category':Category.objects.all(),
        'books':Book.objects.all(),
        'form': BookForm(),
        'formCat':CategoryForm(),
        'allbooks':Book.objects.filter(active=True).count(),
        'availableBooks':Book.objects.filter(state='available').count(),
        'rentalBooks':Book.objects.filter(state='rental').count(),
        'soldBooks':Book.objects.filter(state='sold').count(),
    }
    return render(request,'pages/index.html',context)

def books (request):
    search= Book.objects.all()
    author = None
    title = None
    if 'search_name' in request.GET :
        title = request.GET['search_name']
        author = request.GET['search_name']
        if title :
            search_title = search.filter(title__icontains=title)
        if author :    
            search_author = search.filter(author__icontains=author)
        if search_title != None:
                search=search_title
        else:
                search = search_author

    context={
        'category':Category.objects.all(),
        'books':search,
        'formCat':CategoryForm(),
    }
    return render(request,'pages/books.html',context)

def update (request,id):
    book_id = Book.objects.get(id = id)
    if request.method =='POST':
        book_save=BookForm(request.POST,request.FILES,instance = book_id)
        if book_save.is_valid():
            book_save.save()
            return redirect('/')
    else:
        book_save=BookForm(instance = book_id)
    context = {
        'form':book_save,
    }            
    return render(request,'pages/update.html',context)

def delete (request,id):
    book_delete = get_object_or_404(Book,id=id)
    if request.method =='POST':
        book_delete.delete()
        return redirect('/')
    return render(request,'pages/delete.html')    
            