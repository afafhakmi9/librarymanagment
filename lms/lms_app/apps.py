from django.apps import AppConfig
#from Suit.apps import DjangoSuitCinfig

class LmsAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'lms_app'
# class SuitConfig (DjangoSuitCinfig):
#     layout = 'horizontal'
