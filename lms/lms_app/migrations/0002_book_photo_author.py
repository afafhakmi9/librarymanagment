# Generated by Django 4.0 on 2021-12-18 11:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lms_app', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='book',
            name='photo_author',
            field=models.ImageField(blank=True, null=True, upload_to='photo'),
        ),
    ]
