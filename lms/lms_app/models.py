from django.db import models

# Create your models here.


class Category (models.Model):
    name=models.CharField(max_length=50)

    def __str__(self):
        return self.name

class Book(models.Model):

    book_status= [
        ('available','available'),
        ('rental','rental'),
        ('sold','sold'),
    ]

    title=models.CharField(max_length=50)
    author = models.CharField(max_length=50)
    photo_book = models.ImageField(upload_to='photo',null=True,blank=True)
    photo_author = models.ImageField(upload_to='photo',null=True,blank=True)
    pages=models.IntegerField()
    price=models.DecimalField(max_digits=10000,decimal_places=2,null=True,blank=True)
    retal_price_day=models.DecimalField(max_digits=10000,decimal_places=2,null=True,blank=True)
    retal_period=models.IntegerField(null=True,blank=True)
    total_days = models.DecimalField(max_digits=10000,decimal_places=2,null=True,blank=True)
    active = models.BooleanField(default=True)
    state = models.CharField(max_length=100,choices=book_status,null=True,blank=True)
    category = models.ForeignKey(Category,on_delete=models.PROTECT)

    def __str__(self):
        return self.title







